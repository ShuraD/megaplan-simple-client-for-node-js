const axios = require('axios');

class Client {
    constructor(host, user, password) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.accessToken = null;
    }
    auth() {
        let self = this;
        return new Promise(async (resolve, reject) => {
            try {
                const response = await axios({
                    method: 'post',
                    url: self.host + '/api/v3/auth/access_token',
                    headers: {
                        'Content-Type': 'application/json;'
                    },
                    data: {
                        username: self.user,
                        password: self.password,
                        grant_type: 'password',
                    },
                });
                if (response['data']['access_token']) {
                    self.accessToken = response['data']['access_token'];
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    }
    get(url) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            try {
                if (!self.accessToken) {
                    await self.auth();
                }
                const response = await axios({
                    method: 'get',
                    url: self.host + url,
                    headers: {
                        'AUTHORIZATION': 'Bearer ' + self.accessToken
                    },
                });
                resolve(response);
            } catch (e) {
                reject(e);
            }
        });
    }
    post(url, data) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            try {
                if (!self.accessToken) {
                    await self.auth();
                }
                const response = await axios({
                    method: 'post',
                    url: self.host + url,
                    data: data,
                    headers: {
                        'Content-Type': 'application/json;',
                        'AUTHORIZATION': 'Bearer ' + self.accessToken
                    },
                });
                resolve(response);
            } catch (e) {
                reject(e);
            }
        });
    }
    delete(url) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            try {
                if (!self.accessToken) {
                    await self.auth();
                }
                const response = await axios({
                    method: 'delete',
                    url: self.host + url,
                    headers: {
                        'AUTHORIZATION': 'Bearer ' + self.accessToken
                    },
                });
                resolve(response);
            } catch (e) {
                reject(e);
            }
        });
    }
}

module.exports = Client;